package rice;

public class Node {
    public Node Next;
    public Object Data;

    public Node(Object data){
        this.Data = data;
        Next = null;
    }
}
