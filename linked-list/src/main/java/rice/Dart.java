package rice;

public class Dart {

    public double X;
    public double Y;

    public Boolean Throw() {

        X = Math.random(); // returns a value between 0 & 1
        Y = Math.random(); // also between zero and one

        // is the dart inside the circle?
        return X * X + Y * Y <= 1;

    }

}
