package rice;

public class PiEstimator {

    public double Value(int numberOfThrows) {

        // we have a square board of side length two with a circle
        // wrapped inside it with radius 1.

        // we know the probablity of getting a dart inside a circle of radius
        // one is pi/4 because:

        // 1- the area of a circle with radius 1 is
        // pi * 1 * 1 = pi
        // 2- The area of a square with a side of length 2 is 2*2 = 4
        // probablity of throwing a dart and having it lie inside the circle
        // of radius one is area of circle / area of square = pi/4

        Dart dart;
        double countOfHits = 0;
        Boolean isHit;

        for (int i = 0; i < numberOfThrows; i++) {
            dart = new Dart();
            isHit = dart.Throw();
            if (isHit)
                countOfHits++;

        }

        double probablityOfHits = countOfHits/(double)numberOfThrows;

        return probablityOfHits * 4;
    }

}
