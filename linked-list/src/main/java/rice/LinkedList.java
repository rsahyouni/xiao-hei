/* this is an important class by 小米 */
/*  家丑不可外扬 */
package rice;

public class LinkedList {

    private Node _head;

    private int _count;

    public LinkedList() {
        _head = null;
    }

    public LinkedList(Node head){
        _head = head;
    }

    /*
     * 1- we are starting with some List: [Node1]-> [Node2]->[Node3] 2- insert a new
     * 2- we want to insert a new node at the begining of the list:
     * [item]->[Node1]-> [Node2]->[Node3]
     */

    public void InsertAtFront(Object item) {
        Node temp = new Node(item);
        temp.Next = _head;
        _head = temp;
        _count++;
    }

    /* appends a new node at the end of the list */
    public void Append(Object item) {

        // create a new node to insert
        Node newNode = new Node(item);

        // If the Linked List is empty, then make the new node as head
        if (_count == 0) {
            InsertAtFront(item);
        }

        else {

            // This new node is going to be the last node, set its next to null
            newNode.Next = null;

            // else traverse till the last node
            Node last = _head;
            while (last.Next != null)
                last = last.Next;

            // update the next of last node
            last.Next = newNode;
        }
    }

    /*
     * This function prints contents of linked list starting from the given node
     */
    public String DisplayList() {

        String result = "";
        Node temp = _head;

        while (temp != null) {
            result += "-" + temp.Data + "-";
            temp = temp.Next;
        }

        return result;
    }

    public int GetCount() {

        Node temp = _head;
        _count = 0;

        while (temp != null) {
            _count++;
            temp = temp.Next;
        }

        return _count;
    }

    public int GetCountByRecursion() {

        if (_head == null)
            return 0;

        // head is not null but its next is null => there is one element
        if (_head.Next == null)
            return 1;

        // _head is not null and it has a next
        // create another list that starts with _head.Next
        LinkedList subList = new LinkedList(_head.Next);

        // count is 1 + count of remaining list
        return 1 + subList.GetCountByRecursion();
    }
}
