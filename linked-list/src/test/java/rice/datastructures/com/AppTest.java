package rice.datastructures.com;

import org.junit.jupiter.api.Test;

import rice.LinkedList;
import rice.Node;
import rice.PiEstimator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * author 小米
 */
class AppTest {

    @Test
    void Create_Node_With_Integer() {

        // create a target instance
        Node target = new Node(1);

        // get the actual result
        Object actual = target.Data;

        // assert expected equals actual
        assertEquals(target.Data, actual);
    }

    @Test
    void Create_Node_With_String() {

        Node target = new Node("说曹操，曹操就到");

        assertEquals(target.Data, "说曹操，曹操就到");
    }

    @Test
    void Get_Count_Empty_List_Returns_Zero() {

        LinkedList target = new LinkedList();
        assertEquals(0, target.GetCount());
    }

    @Test
    void Display_Empty_List() {

        LinkedList target = new LinkedList();

        // act
        String actual = target.DisplayList();

        // assert
        assertEquals("", actual);
    }

    @Test
    void InsertAtFront_Inserts_One_Integer() {

        LinkedList target = new LinkedList();

        target.InsertAtFront(1);

        String actual = target.DisplayList();
        // assert
        assertEquals("-1-", actual);
    }

    @Test
    void InsertAtFront_Inserts_One_String() {

        LinkedList target = new LinkedList();

        target.InsertAtFront("夏夏");

        String actual = target.DisplayList();
        // assert
        assertEquals("-夏夏-", actual);
    }

    @Test
    void Append_Three_Items() {

        LinkedList target = new LinkedList();

        // append once
        target.Append("夏夏");

        String actual = target.DisplayList();

        // assert
        assertEquals("-夏夏-", actual);

        // append another one
        target.Append("another one");

        actual = target.DisplayList();

        // assert
        assertEquals("-夏夏--another one-", actual);

        // append a third time
        target.Append("有钱就是任性");

        actual = target.DisplayList();
        // assert
        assertEquals("-夏夏--another one--有钱就是任性-", actual);
    }

    @Test
    void Count_After_Append_Three_Items_Then_InsertAtFront() {

        LinkedList target = new LinkedList();

        // append once
        target.Append("夏夏");
        assertEquals(1, target.GetCount());

        // append another one
        target.Append("another one");
        assertEquals(2, target.GetCount());

        // append a third time
        target.Append("有钱就是任性");
        assertEquals(3, target.GetCount());


        target.InsertAtFront("Duck Sauce");
        assertEquals(4, target.GetCount());
    }


    @Test
    void CountByRecursion_After_Append_Three_Items_Then_InsertAtFront() {

        LinkedList target = new LinkedList();

        // append once
        target.Append("夏夏");
        assertEquals(1, target.GetCountByRecursion());

        // append another one
        target.Append("another one");
        assertEquals(2, target.GetCountByRecursion());

        // append a third time
        target.Append("有钱就是任性");
        assertEquals(3, target.GetCountByRecursion());


        target.InsertAtFront("Duck Sauce");
        assertEquals(4, target.GetCountByRecursion());
    }


    @Test
    void Estimate_Pi_With_More_Throws() {

        PiEstimator target = new PiEstimator();

        double Pi = target.Value(100000);
        assertTrue(Pi > 3.13);

        assertTrue(Pi < 3.15);
    }

}
